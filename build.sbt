name := "KMeans"

version := "1.0"

scalaVersion := "2.10.2"

scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature")

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.2.3"

libraryDependencies += "com.typesafe.akka" %% "akka-cluster" % "2.2.3"

libraryDependencies += "com.typesafe.akka" %% "akka-kernel" % "2.2.3"

libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.0.M8" % "test"

distSettings

distJvmOptions in Dist += " -Xmx1500M"

scalariformSettings
