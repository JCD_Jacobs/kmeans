package io

import scala.io.Source
import scala.math.pow
import scala.math.sqrt

object InputReader {

  private def zScoreTransform(vectors: Array[Array[Double]]): Array[Array[Double]] = {

    def extractMoments(valuesVectorX: Traversable[Double], numMoments: Int, numObjs: Int): Seq[Double] = {
      val moments = Array.ofDim[Double](numMoments)
      val moments0 = valuesVectorX.sum / numObjs
      moments(0) = moments0

      (1 until numMoments).foreach(
        (i: Int) => {
          moments(i) = valuesVectorX.map(
            (value: Double) => pow(value - moments0, i + 1)
          ).sum / numObjs
        }
      )
      moments
    }

    val numVecs = vectors.size

    vectors.transpose.map(
      (valuesVectorX) => {
        val moments = extractMoments(valuesVectorX, 2, numVecs)
        val moment0 = moments(0) // mean of vectorX
        val sqrtMoment1 = sqrt(moments(1)) // sqrt of mean of square-difference to mean

        valuesVectorX.map(
          (value: Double) => (value - moment0) / sqrtMoment1
        )
      }
    ).transpose
  }

  private def read(filePath: String): Array[Array[Double]] = {
    val file = Source.fromFile(filePath)
    file.getLines().map(
      (line: String) => line.trim.split(' ').tail.map(_.toDouble)
    ).toArray
  }

  def readASCIIFile(filePath: String, zScore: Boolean): Array[Array[Double]] = {

    if (zScore) {
      println("Read and zScoreTransform: %s".format(filePath))
      zScoreTransform(read(filePath))
    } else {
      println("Read: %s".format(filePath))
      read(filePath)
    }
  }
}
