package actors.protocols

import java.net.InetSocketAddress
import akka.io.Tcp.Event

object MasterWorker {

  // Messages to Worker
  case class VectorsMessage(vectorsSeq: Seq[Seq[Double]])

  case class VectorsServerMessage(hostname: String, port: Int)

  case class IterateMessage(iteration: Int, clusterCentersSeq: Seq[Seq[Double]])

  // Messages from Worker
  case class GetVectorsMessage(startID: Int, stopID: Int)

  case class IntermediateResultMessage(iteration: Int, absoluteDelta: Int, clusterMemberCountSeq: Seq[Int], vectorSumsSeq: Seq[Seq[Double]])

  case class AbortMessage(reason: Throwable)

  case object ReadyMessage // only for measurements
}
