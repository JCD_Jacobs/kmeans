package actors.protocols

import akka.actor.ActorRef

object NodeActorFactory {

  // Messages to Factory
  case class GetNodeActorMessage(nodeID: Int, firstVectorID: Int, lastVectorID: Int, numClusters: Int,
    initialClusterCentersSeq: Seq[Seq[Double]], iteration: Int)

  // Messages from Factory
  case class NewNodeActorMessage(nodeID: Int, node: ActorRef)
}
