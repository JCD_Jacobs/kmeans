package actors.helpers

import scala.annotation.tailrec
import actors.protocols.MasterWorker.IntermediateResultMessage

class IntermediateResultCombiner(numWorkers: Int, numClusters: Int, numDims: Int) {
  private var combineDelta: Int = 0
  private val combineClusterMemberCount: Array[Int] = Array.fill(numClusters)(0)
  private val combineVectorSums: Array[Array[Double]] = Array.ofDim[Double](numClusters, numDims)
  private var readyCountdown = numWorkers

  private def addClusterMemberCount(clusterMemberCount: Array[Int]) = {
    val stopID = clusterMemberCount.length
    @tailrec
    def _addClusterMemberCount(indexCluster: Int): Unit = {
      if (indexCluster < stopID) {
        combineClusterMemberCount(indexCluster) += clusterMemberCount(indexCluster)
        _addClusterMemberCount(indexCluster + 1)
      }
    }
    _addClusterMemberCount(0)
  }

  private def addVectorSums(vectorSums: Array[Array[Double]]) = {
    val stopCluster = vectorSums.length
    val stopVector = vectorSums(0).length

    @tailrec
    def _iterateClusters(indexCluster: Int): Unit = {
      if (indexCluster < stopCluster) {
        _iterateDims(indexCluster)
        _iterateClusters(indexCluster + 1)
      }
    }

    @tailrec
    def _iterateDims(indexCluster: Int, indexDim: Int = 0): Unit = {
      if (indexDim < stopVector) {
        combineVectorSums(indexCluster)(indexDim) += vectorSums(indexCluster)(indexDim)
        _iterateDims(indexCluster, indexDim + 1)
      }
    }

    _iterateClusters(0)
  }

  def resultReady(): Boolean = {
    readyCountdown == 0
  }

  def add(intermediateResult: IntermediateResultMessage) = {
    assert(readyCountdown > 0) //"result is complete already"
    combineDelta += intermediateResult.absoluteDelta
    addClusterMemberCount(intermediateResult.clusterMemberCountSeq.toArray)
    addVectorSums(intermediateResult.vectorSumsSeq.view.map((obj: Seq[Double]) => obj.toArray).toArray)
    readyCountdown -= 1
  }

  def getAbsoluteDelta(): Int = {
    assert(readyCountdown == 0) //"result is NOT complete"
    combineDelta
  }

  def getClusterMemberCount(): Seq[Int] = {
    assert(readyCountdown == 0)
    combineClusterMemberCount.toIndexedSeq
  }

  def getClusterMemberCountArray(): Array[Int] = {
    assert(readyCountdown == 0)
    combineClusterMemberCount
  }

  def getVectorSums(): Seq[Seq[Double]] = {
    assert(readyCountdown == 0)
    combineVectorSums.view.toIndexedSeq.map((a: Array[Double]) => a.toSeq)
  }

  def getVectorSumsArray(): Array[Array[Double]] = {
    assert(readyCountdown == 0)
    combineVectorSums
  }
}
