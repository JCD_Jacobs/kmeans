package actors

import protocols.NodeActorFactory._
import akka.actor.{ ActorLogging, OneForOneStrategy, Props, Actor }
import com.typesafe.config.ConfigFactory
import akka.actor.SupervisorStrategy.{ Restart, Stop }

class NodeActorFactoryActor() extends Actor with ActorLogging {
  val numWorkers = ConfigFactory.load().getInt("numWorkers")

  /** Error Handling of NodeActors */
  override val supervisorStrategy = OneForOneStrategy() {
    case _: OutOfMemoryError => Stop
    case _: Exception => Restart
  }

  def receive = {
    case GetNodeActorMessage(nodeActorID, firstVectorID, lastVectorID, numClusters, initialClusterCentersSeq, iteration) => {
      val newNodeActor = context.actorOf(Props(classOf[NodeActor],
        nodeActorID, firstVectorID, lastVectorID, numClusters, iteration, initialClusterCentersSeq, numWorkers, sender))
      sender ! NewNodeActorMessage(nodeActorID, newNodeActor)
    }
  }

}