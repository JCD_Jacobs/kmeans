package actors

import akka.actor.{ ActorRef, ActorLogging, Actor }
import akka.io.{ IO, Tcp }
import java.net.InetSocketAddress
import akka.util.ByteString
import actors.protocols.MasterWorker._
import java.nio.ByteOrder.LITTLE_ENDIAN
import com.typesafe.config.ConfigFactory

class DataServerActor(vectors: Array[Array[Double]], client: ActorRef) extends Actor with ActorLogging {

  import Tcp._
  import context.system
  implicit val byteOrder = LITTLE_ENDIAN
  case object Ack extends Event

  val hostname = ConfigFactory.load().getString("akka.remote.netty.tcp.hostname")
  IO(Tcp) ! Bind(self, new InetSocketAddress(hostname, 0))

  def receive = {
    case b @ Bound(local) =>
      client ! VectorsServerMessage(hostname, local.getPort)

    case CommandFailed(b: Bind) =>
      log.error("DataServerActor bind to {} failed", b)
      context.stop(self)

    case c @ Connected(clientAddress, serverAddress) =>
      val connection = sender
      context.become(connected(clientAddress))
      connection ! Register(self, keepOpenOnPeerClosed = true)

      // send vectors via TCP
      val builder = ByteString.newBuilder
      vectors.map((vec) => builder.putDoubles(vec))
      val data = builder.result()
      val sizeBuilder = ByteString.newBuilder
      val dataSize = sizeBuilder.putInt(data.size + 4).result()
      connection ! Write(dataSize ++ data, Ack)
  }

  def connected(clientAddress: InetSocketAddress): Receive = {
    case Ack =>
      sender ! Close // all data transferred, close connection
    case CommandFailed(w: Write) =>
      log.error("DataServerActor write to {} failed", clientAddress)
    case _: ConnectionClosed =>
      context.stop(self)
  }
}