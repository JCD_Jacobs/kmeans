package actors

import protocols.MasterWorker._
import engines.ClusterEngine
import akka.actor.Actor

class WorkerActor(firstObjID: Int, vectorsSeq: Seq[Seq[Double]], numClusters: Int) extends Actor {

  val vectors = vectorsSeq.map((s: Seq[Double]) => s.toArray).toArray
  val numDims = vectors(0).length
  var membership = Array.fill(vectorsSeq.length)(-1)

  def receive = {
    case IterateMessage(iteration, clusterCentersSeq) => {
      val newMembership = ClusterEngine.assignVectorsToClusters(vectors, clusterCentersSeq.map((v: Seq[Double]) => v.toArray).toArray)
      val clusterMemberCount = ClusterEngine.getClusterMemberCount(newMembership, numClusters)
      val vectorSums = ClusterEngine.sumVectors(vectors, newMembership, numClusters, numDims)
      val absoluteDelta = ClusterEngine.getAbsoluteDelta(membership, newMembership)

      sender ! IntermediateResultMessage(iteration, absoluteDelta, clusterMemberCount, vectorSums.map(_.toIndexedSeq)) // the arrays are converted implicitly

      membership = newMembership
    }
  }
}