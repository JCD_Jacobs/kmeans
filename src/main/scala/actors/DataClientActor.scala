package actors

import akka.actor.{ PoisonPill, ActorLogging, Actor }
import akka.io.{ IO, Tcp }
import java.net.InetSocketAddress
import java.nio.ByteOrder.LITTLE_ENDIAN
import com.typesafe.config.ConfigFactory
import protocols.MasterWorker._
import akka.util.ByteString

class DataClientActor(serverHostname: String, serverPort: Int, numVectors: Int, numDims: Int) extends Actor with ActorLogging {

  import Tcp._
  import context.system
  implicit val byteOrder = LITTLE_ENDIAN

  var storage: ByteString = ByteString.empty

  val localHostname = ConfigFactory.load().getString("akka.remote.netty.tcp.hostname")
  val localAddress = new InetSocketAddress(localHostname, 0)
  val serverAddress = new InetSocketAddress(serverHostname, serverPort)
  IO(Tcp) ! Connect(serverAddress, Option(localAddress))

  def receive = {
    case CommandFailed(_: Connect) =>
      log.error("connection to {} failed", serverAddress)
      context.stop(self)

    case c @ Connected(remote, local) =>
      context.become(connected(remote))
      val connection = sender
      connection ! Register(self)
  }

  def connected(remote: InetSocketAddress): Receive = {
    case _: ConnectionClosed =>
      // all data transferred
      try {
        val iter = storage.iterator
        val dataSize = iter.getInt
        assert(storage.size == dataSize)
        val vectors = Array.ofDim[Double](numVectors, numDims)
        vectors.map((vec: Array[Double]) => iter.getDoubles(vec, 0, numDims))
        // send vectors to NodeActor
        context.parent ! VectorsMessage(vectors.toIndexedSeq.map((v: Array[Double]) => v.toSeq))
      } catch {
        case e: Exception =>
          log.error(e, "Sending vectors over TCP failed")
          context.parent ! PoisonPill
      } finally {
        context.stop(self)
      }

    case Received(data) =>
      storage ++= data
  }
}
