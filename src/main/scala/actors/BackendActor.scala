package actors

import akka.actor._
import actors.protocols.Backend._
import akka.routing.FromConfig
import akka.cluster.{ Cluster, Member }
import akka.cluster.ClusterEvent._
import akka.actor.SupervisorStrategy.Stop

class BackendActor() extends Actor with ActorLogging {

  var jobs = Map[Int, JobMessage]()
  var jobClientMap = Map[Int, ActorRef]()
  var observers = Set[ActorRef]()
  var clusterMembers = Map[Address, Member]()

  val nodeActorFactoryRouter = context.actorOf(Props.empty.withRouter(FromConfig), name = "nodeActorFactoryRouter")

  /** Error Handling of MasterActors */
  override val supervisorStrategy = OneForOneStrategy() {
    case e: Exception => {
      log.error("A MasterActor threw an exception.")
      val jobID = sender.hashCode()
      jobClientMap(jobID) ! ResultMessage(jobID, Seq[Seq[Double]](), -2, 0, 0, 0, 0)
      jobs -= jobID
      jobClientMap -= jobID
      distributeBackendStatus()
      Stop
    }
  }

  override def preStart() = {
    Cluster(context.system).subscribe(self, classOf[ClusterDomainEvent])
  }

  def distributeClusterStatus() = {
    val newStatus = ClusterStatusMessage(clusterMembers.values.toList)
    observers.foreach(_ ! newStatus)
  }

  def distributeBackendStatus() = {
    val backendStatus = BackendStatusMessage(jobs)
    observers.foreach(_ ! backendStatus)
  }

  def receive = {

    case RegisterAsObserverMessage => {
      context.watch(sender)
      observers += sender
      sender ! ClusterStatusMessage(clusterMembers.values.toList)
      sender ! BackendStatusMessage(jobs)
    }

    // cluster status
    case state: CurrentClusterState => {
      for (member <- state.members) {
        clusterMembers += (member.address -> member)
      }
      distributeClusterStatus()
    }
    case MemberUp(member) => {
      clusterMembers += (member.address -> member)
      distributeClusterStatus()
    }
    case UnreachableMember(member) => {
      clusterMembers += (member.address -> member)
      distributeClusterStatus()
    }
    case MemberRemoved(member, previousStatus) => {
      clusterMembers -= member.address
      distributeClusterStatus()
    }
    // end cluster status

    case jobMessage: JobMessage => {
      // start a MasterActor
      val masterActorRef = context.actorOf(Props(classOf[actors.MasterActor], jobMessage.numNodes,
        jobMessage.numClusters, jobMessage.threshold, jobMessage.inputFilePath, jobMessage.zScoreTransform, nodeActorFactoryRouter))

      val jobID = masterActorRef.hashCode()
      jobs += (jobID -> jobMessage)
      jobClientMap += (jobID -> sender)
      distributeBackendStatus()
    }

    case result: ResultMessage => {
      // stop masterActor
      sender ! PoisonPill

      jobClientMap(result.jobID) ! result

      jobs -= result.jobID
      jobClientMap -= result.jobID
      distributeBackendStatus()
    }

    case terminated: Terminated => {
      observers -= terminated.getActor()
    }
  }

}
