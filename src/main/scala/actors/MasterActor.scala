package actors

import protocols.MasterWorker._
import protocols.NodeActorFactory._
import protocols.Backend._
import helpers.IntermediateResultCombiner
import io.InputReader
import engines.ClusterEngine
import akka.actor._
import scala.compat.Platform

class MasterActor(numNodes: Int, numClusters: Int, threshold: Double, inputFilePath: String,
    zScoreTransform: Boolean, nodeActorFactoryActor: ActorRef) extends Actor with ActorLogging {

  val nodeActors: Array[ActorRef] = Array.fill(numNodes)(context.system.deadLetters)

  val time1 = Platform.currentTime
  val vectors = InputReader.readASCIIFile(inputFilePath, zScoreTransform)
  val time2 = Platform.currentTime
  var time3 = 0L
  var time4 = 0L
  var time5 = 0L

  val numVectors = vectors.length
  val numDims = vectors(0).length

  var clusterCenters: Array[Array[Double]] = ClusterEngine.getInitialClusterCenters(numClusters, vectors)
  var masterIteration: Int = 0
  var deltaSum: Double = 0.0F
  var combiner: IntermediateResultCombiner = new IntermediateResultCombiner(numNodes, numClusters, numDims)

  override def preStart() {
    time3 = Platform.currentTime
    (0 to (numNodes - 1)).foreach(nodeActorID => callForNodeActor(nodeActorID))
  }

  def receive = {
    // NodeActor error detection
    case Terminated(deadNodeActor) =>
      val nodeActorID = nodeActors.indexOf(deadNodeActor)
      callForNodeActor(nodeActorID)
      log.error("\n\tnodeActor %d has died and will be replaced with a fresh nodeActor".format(nodeActorID))

    case NewNodeActorMessage(nodeID, node) =>
      log.info("\n\tnode %d initializing\n".format(nodeID))
      nodeActors(nodeID) = node
      context.watch(node) // activate DeathWatch

    case GetVectorsMessage(startID, stopID) =>
      val vecs = vectors.slice(startID, stopID + 1)
      //local
      //sender ! VectorsMessage(vecs.toIndexedSeq.map((v: Array[Double]) => v.toSeq))
      //remote
      context.actorOf(Props(classOf[DataServerActor], vecs, sender))

    case ReadyMessage =>
      time4 = Platform.currentTime

    case intermediateResult: IntermediateResultMessage =>
      if (intermediateResult.iteration == masterIteration) {
        combiner.add(intermediateResult)

        if (combiner.resultReady()) {
          val newClusterCenters = ClusterEngine.getNewClusterCenters(combiner.getVectorSumsArray(),
            combiner.getClusterMemberCountArray(), clusterCenters, numClusters, numDims)
          val delta = combiner.getAbsoluteDelta().toDouble / numVectors
          deltaSum += delta
          masterIteration += 1
          clusterCenters = newClusterCenters

          if ((delta > threshold) && (masterIteration < 500)) {
            // continue clustering
            combiner = new IntermediateResultCombiner(numNodes, numClusters, numDims)
            nodeActors.foreach(_ ! IterateMessage(masterIteration, newClusterCenters.toIndexedSeq.map((a: Array[Double]) => a.toSeq)))
          } else {
            // done
            time5 = Platform.currentTime
            log.info("\n\tclustering complete\n")

            context.parent ! ResultMessage(self.hashCode(), newClusterCenters.toIndexedSeq.map((a: Array[Double]) => a.toSeq),
              masterIteration - 1, deltaSum, time2 - time1, time4 - time3, time5 - time4)
          }
        }
      } else {
        log.warning("\n\told message received (MasterActor)")
      }

    case AbortMessage(reason) =>
      log.error(reason, "A fatal error occurred in a WorkerActor or NodeActor. Look at the logs of the remote nodes, too.")
      context.parent ! ResultMessage(self.hashCode(), Seq[Seq[Double]](), -1, 0, 0, 0, 0)
      context.stop(self)

  }

  def callForNodeActor(nodeActorID: Int) {
    assert(nodeActorID >= 0 && nodeActorID < nodeActors.length)
    val clusterCentersSeq = clusterCenters.toIndexedSeq.map((a: Array[Double]) => a.toSeq)

    val firstVectorID = nodeActorID * (numVectors / numNodes)
    val lastVectorID = if (nodeActorID < numNodes - 1) {
      firstVectorID + numVectors / numNodes - 1
    } else {
      numVectors - 1
    }
    nodeActorFactoryActor ! GetNodeActorMessage(nodeActorID, firstVectorID, lastVectorID, numClusters, clusterCentersSeq, masterIteration)
  }
}
