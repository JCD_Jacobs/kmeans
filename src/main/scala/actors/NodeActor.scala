package actors

import protocols.MasterWorker._
import helpers.IntermediateResultCombiner
import akka.actor._
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy._

class NodeActor(nodeID: Int, firstVectorID: Int, lastVectorID: Int, numClusters: Int,
    initialIteration: Int, initialClusterCentersSeq: Seq[Seq[Double]],
    numWorkers: Int, master: ActorRef) extends Actor with ActorLogging {

  var workerActors: Seq[ActorRef] = Seq(context.system.deadLetters)

  val numVectors = lastVectorID - firstVectorID + 1
  val numDims = initialClusterCentersSeq(0).length

  var combiner: IntermediateResultCombiner = if (numWorkers > 1) new IntermediateResultCombiner(numWorkers, numClusters, numDims) else null
  var nodeIteration = initialIteration
  var clusterCenters = initialClusterCentersSeq

  /** Error Handling of WorkerActors */
  override val supervisorStrategy = OneForOneStrategy() {
    case _: OutOfMemoryError => Escalate
    case e: Exception =>
      // program is deterministic, therefore a restart would be pointless
      master ! AbortMessage(e)
      Stop
  }

  override def preStart() = {
    context.watch(master)
    master ! GetVectorsMessage(firstVectorID, lastVectorID)
  }

  override def preRestart(reason: Throwable, message: Option[Any]) = {
    // an error occurred, the program is deterministic, therefore a restart would be pointless
    master ! AbortMessage(reason)
  }

  def receive = {

    case Terminated(deadMasterActor) =>
      context.stop(self)

    case VectorsMessage(vectorsSeq) =>
      master ! ReadyMessage

      context.become(ready) // change behavior
      val partitionSize = numVectors / numWorkers
      var startID = 0
      // create workers
      workerActors = vectorsSeq.grouped(partitionSize).map(
        (vectorsSlice: Seq[Seq[Double]]) => {
          val worker = context.actorOf(Props(classOf[WorkerActor], startID, vectorsSlice, numClusters))
          startID += partitionSize
          worker
        }).toSeq

      workerActors.foreach(worker => worker ! IterateMessage(nodeIteration, clusterCenters)) // start processing
      log.info("\n\tnode %d starts clustering\n".format(nodeID))

    case VectorsServerMessage(serverHostname, serverPort) =>
      context.actorOf(Props(classOf[DataClientActor], serverHostname, serverPort, numVectors, numDims))

  }

  def ready: Receive = {

    case Terminated(deadMasterActor) =>
      context.stop(self)

    case IterateMessage(iteration, newClusterCentersSeq) =>
      nodeIteration = iteration
      clusterCenters = newClusterCentersSeq
      if (numWorkers > 1) {
        combiner = new IntermediateResultCombiner(numWorkers, numClusters, numDims)
      }
      workerActors.foreach(_ ! IterateMessage(iteration, newClusterCentersSeq))

    case intermediateResult: IntermediateResultMessage =>
      if (intermediateResult.iteration == nodeIteration) {
        if (numWorkers > 1) {
          combiner.add(intermediateResult)
          if (combiner.resultReady()) {
            master ! IntermediateResultMessage(nodeIteration, combiner.getAbsoluteDelta(), combiner.getClusterMemberCount(), combiner.getVectorSums())
          }
        } else {
          master ! intermediateResult
        }
      } else {
        log.warning("\n\told message received(NodeActor)")
      }

  }
}
