package engines

import scala.annotation.tailrec
import random.MersenneTwisterRandom

object ClusterEngine {

  def getNewClusterCenters(vectorSums: Array[Array[Double]], clusterMemberCount: Array[Int], oldClusterCenters: Array[Array[Double]], numClusters: Int, numDims: Int): Array[Array[Double]] = {

    @tailrec
    def getCenter(vectorSum: Array[Double], memberCount: Int, dimIndex: Int = 0, newCenter: Array[Double] = Array.ofDim[Double](numDims)): Array[Double] = {
      if (dimIndex < numDims) {
        newCenter(dimIndex) = vectorSum(dimIndex) / memberCount
        getCenter(vectorSum, memberCount, dimIndex + 1, newCenter)
      } else {
        newCenter
      }
    }

    @tailrec
    def iterate(index: Int = 0, newClusterCenters: Array[Array[Double]] = Array.ofDim[Array[Double]](numClusters)): Array[Array[Double]] = {
      if (index < numClusters) {
        if (clusterMemberCount(index) > 0) {
          newClusterCenters(index) = getCenter(vectorSums(index), clusterMemberCount(index))
        } else {
          newClusterCenters(index) = oldClusterCenters(index)
        }
        iterate(index + 1, newClusterCenters)
      } else {
        newClusterCenters
      }
    }

    iterate()
  }

  def getInitialClusterCenters(numClusters: Int, vectors: Array[Array[Double]]): Array[Array[Double]] = {
    /*
    import scala.util.Random
    val random = new Random(7)
    val randomIndexes = Array.fill(numClusters)(random.nextInt(vectors.length))
    */

    // the STAMP random function
    import scala.math.abs
    val random = new MersenneTwisterRandom()
    random.random_alloc()
    random.random_seed(7)
    val randomIndexes = Array.fill(numClusters)(abs(random.random_generate()) % vectors.length)

    randomIndexes.map(vectors(_))
  }

  def sumVectors(vectors: Array[Array[Double]], memberships: Array[Int], numClusters: Int, numDims: Int): Array[Array[Double]] = {

    @tailrec
    def addToMemberSum(ob: Array[Double], sum: Array[Double], dimIndex: Int = 0): Unit = {
      if (dimIndex < numDims) {
        sum(dimIndex) += ob(dimIndex)
        addToMemberSum(ob, sum, dimIndex + 1)
      }
    }

    @tailrec
    def iterate(vectorIndex: Int = 0, vectorSums: Array[Array[Double]] = Array.fill(numClusters)(Array.fill(numDims)(0.0f))): Array[Array[Double]] = {
      if (vectorIndex < vectors.length) {
        addToMemberSum(vectors(vectorIndex), vectorSums(memberships(vectorIndex)))
        iterate(vectorIndex + 1, vectorSums)
      } else {
        vectorSums
      }
    }

    iterate()
  }

  def getClusterMemberCount(memberships: Array[Int], numClusters: Int): Array[Int] = {

    @tailrec
    def iterate(membershipsIndex: Int = 0, memberCount: Array[Int] = Array.fill(numClusters)(0)): Array[Int] = {
      if (membershipsIndex < memberships.length) {
        memberCount(memberships(membershipsIndex)) += 1
        iterate(membershipsIndex + 1, memberCount)
      } else {
        memberCount
      }
    }

    iterate()
  }

  def getAbsoluteDelta(oldMembership: Array[Int], newMembership: Array[Int]): Int = {
    val numVectors = oldMembership.length

    @tailrec
    def iterate(delta: Int = 0, index: Int = 0): Int = {
      if (index < numVectors) {
        if (oldMembership(index) != newMembership(index)) {
          iterate(delta + 1, index + 1)
        } else {
          iterate(delta, index + 1)
        }
      } else {
        delta
      }
    }

    iterate()
  }

  def assignVectorsToClusters(vectors: Array[Array[Double]], clustersCenters: Array[Array[Double]]): Array[Int] = {

    @tailrec
    def findNearestCenter(ob: Array[Double], maxDistance: Double = Double.MaxValue, centerIndex: Int = 0, result: Int = -1): Int = {

      if (centerIndex < clustersCenters.length && maxDistance > 0f) {
        val distance = euclidDist2(ob, clustersCenters(centerIndex))
        if ((distance / maxDistance) < 0.99999f) {
          findNearestCenter(ob, distance, centerIndex + 1, centerIndex)
        } else {
          findNearestCenter(ob, maxDistance, centerIndex + 1, result)
        }
      } else {
        result
      }
    }

    @tailrec
    def euclidDist2(ob1: Array[Double], ob2: Array[Double], i: Int = 0, dist2: Double = 0.0f): Double = {

      if (i == ob1.length) {
        dist2
      } else {
        val diff = ob1(i) - ob2(i)
        euclidDist2(ob1, ob2, i + 1, dist2 + (diff * diff))
      }
    }

    vectors.map(findNearestCenter(_))
  }

}
