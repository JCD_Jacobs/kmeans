import actors.{ NodeActorFactoryActor, BackendActor }
import akka.actor.{ ActorSystem, Props }
import akka.kernel.Bootable

class KMeans extends Bootable {

  val _ = setPort()
  val system = ActorSystem("kmeans")

  def startup() = {
    system.actorOf(Props[BackendActor], name = "backendActor")
    system.actorOf(Props[NodeActorFactoryActor], name = "nodeActorFactoryActor")
  }

  def shutdown() = {
    system.shutdown()
  }

  private def setPort() = {
    val port = System.getenv("AKKAPORT")
    if (port.nonEmpty) {
      val portNum = port.toInt
      assert(portNum >= 1024 && portNum <= 65535, "Port must be in range 1024-65535!")
      System.setProperty("akka.remote.netty.tcp.port", port)
    }
  }
}