import io.InputReader
import org.scalatest.{ Matchers, FunSuite }

class TestInputReader extends FunSuite with Matchers {

  test("zScoreTransform") {
    val result = Array(
      Array(-1.4648567, 1.4856074),
      Array(0.8085736, -1.3324759),
      Array(0.68305504, -0.49924478),
      Array(-0.94437486, 0.76817673),
      Array(0.9176031, -0.42206335)
    )
    val vectors = InputReader.readASCIIFile("src/test/resources/color5", zScore = true)

    (vectors.flatten, result.flatten).zipped.foreach((f1, f2) => f1 should be(f2 +- 0.00001))
  }
}
