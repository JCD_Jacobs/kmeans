import engines.ClusterEngine
import org.scalatest.{ Matchers, FunSuite }

class TestClusterEngine extends FunSuite with Matchers {

  test("initial cluster centers") {
    ClusterEngine.getInitialClusterCenters(0, Array(Array[Double]())) should equal(Array())
    ClusterEngine.getInitialClusterCenters(1, Array(Array(1.0))) should equal(Array(Array(1.0)))
    ClusterEngine.getInitialClusterCenters(2, Array(Array(1.0), Array(2.0), Array(3.0))) should equal(Array(Array(2.0), Array(3.0)))
  }

  test("assign vectors to clusters") {
    ClusterEngine.assignVectorsToClusters(Array(Array(0.0)), Array(Array(1.0))) should equal(Array(0))
    ClusterEngine.assignVectorsToClusters(Array(Array(0.0)), Array(Array(1.0), Array(2.0))) should equal(Array(0))
    ClusterEngine.assignVectorsToClusters(Array(Array(0.0)), Array(Array(2.0), Array(1.0))) should equal(Array(1))
    ClusterEngine.assignVectorsToClusters(Array(Array(0.0)), Array(Array(-1.0), Array(1.0))) should equal(Array(0))
    ClusterEngine.assignVectorsToClusters(Array(Array(0.0)), Array(Array(1.0), Array(-1.0))) should equal(Array(0))
    ClusterEngine.assignVectorsToClusters(Array(Array(0.0)), Array(Array(0.00001), Array(0.000001))) should equal(Array(1))
  }

  test("member count") {
    ClusterEngine.getClusterMemberCount(Array(), 0) should equal(Array())
    ClusterEngine.getClusterMemberCount(Array(0, 0, 1, 0), 3) should equal(Array(3, 1, 0))
  }

  test("delta") {
    ClusterEngine.getAbsoluteDelta(Array(), Array()) should equal(0.0)
    ClusterEngine.getAbsoluteDelta(Array(0), Array(0)) should equal(0.0)
    ClusterEngine.getAbsoluteDelta(Array(0), Array(1)) should equal(1.0)
    ClusterEngine.getAbsoluteDelta(Array(0, 0), Array(1, 1)) should equal(2.0)
  }

  test("sum vectors") {
    ClusterEngine.sumVectors(Array(Array(1.0)), Array(0), 1, 1) should equal(Array(Array(1.0)))
    ClusterEngine.sumVectors(Array(Array(1.0), Array(1.0)), Array(0, 0), 1, 1) should equal(Array(Array(2.0)))
    ClusterEngine.sumVectors(Array(Array(1.0), Array(1.0)), Array(0, 1), 2, 1) should equal(Array(Array(1.0), Array(1.0)))
  }

  test("new cluster centers") {
    // cluster has one member, take sum members divided by 1
    ClusterEngine.getNewClusterCenters(Array(Array(1.0)), Array(1), Array(Array(2.0)), 1, 1) should equal(Array(Array(1.0)))
    // cluster has no member, take old center
    ClusterEngine.getNewClusterCenters(Array(Array(1.0)), Array(0), Array(Array(2.0)), 1, 1) should equal(Array(Array(2.0)))
    // cluster has 2 members, take sum members divided by 2
    ClusterEngine.getNewClusterCenters(Array(Array(5.0)), Array(2), Array(Array(2.0)), 1, 1) should equal(Array(Array(2.5)))
    // one vector, two dimensions
    ClusterEngine.getNewClusterCenters(Array(Array(5.0, 4.0)), Array(2), Array(Array(2.0, 1.0)), 1, 2) should equal(Array(Array(2.5, 2.0)))
    // two vectors
    ClusterEngine.getNewClusterCenters(Array(Array(5.0), Array(4.0)), Array(2, 1), Array(Array(2.0), Array(1.0)), 2, 1) should equal(Array(Array(2.5), Array(4.0)))
  }
}
