Requirements
============
* Java Runtime Environment
* Java Development Kit
* Scala Build Tool (sbt)


Configuration
=============
The configuration is in src/main/recources/application.conf

If you want to run kmeans on multiple computers, make shure akka.remote.netty.tcp.hostname and
akka.cluster.seed-nodes are correct.

If you have problems with the java heap size, add for example "-Xmx1500M" to the distJvmOptions in the build.sbt file
and compile again.

Run
===
compile:

        $sbt dist

run:

        $./run.sh [optional port]


Firewall
========
This application listens on port 2553.
The ephemeral ports (32768–65535)? are used for the transfer of the data to the worker nodes.
